/*
 10. Access the login route to test if it's working as intended.
 11. Create a condition for any other routes that will return an error message.
 12. Access any other route to test if it's working as intended.
 
*/
const http = require("http");

const port = 3000;

http.createServer((request, response) =>{
	
	// "request.url" is used to get the request URL ("client route/endpoint")
	if(request.url == '/login'){
		response.writeHead(200, {"Content-type": "text/plain"})
		response.end("Welcome to the login page");
	}
	else if(request.url == '/register'){
		response.writeHead(200, {"Content-type": "text/plain"})
		response.end("I'm sorry the page you are looking for cannot be found.");
	}

}).listen(port);
console.log(`Server now accessible at localhost: ${port}`);

/*
// 1.What directive is used by Node.js in loading the modules it needs?

	Answer: Import directive

// 2.What Node.js module contains a method for server creation.

	Answer: response module

// 3. What is the method of the http object responsible for creating a server using Node.js?

	Answer: createServer

// 4. What method of the response object allows us to set status codes and content types?

	Answer: writeHead

// 5. Where will console.log() output its contents when run in Node.js?

	Answer: browser

// 6. What property of the request object contains the address's endpoint?

	Answer: response end

*/